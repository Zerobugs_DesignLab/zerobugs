<?php  
session_start();
$con = mysqli_connect("localhost", "root", "", "project"); //Connection variable

if(mysqli_connect_errno()) 
{
	echo "Failed to connect: " . mysqli_connect_errno();
}
//Declaring variables to prevent errors
$name = ""; //name
$phone_number = ""; //phonenumber
$id = ""; // id_of_buyer
$locality = ""; // loality 
$em = ""; //email
$em2 = ""; //email 2
$password = ""; //password
$password2 = ""; //password 2
$date = ""; //Sign up date 
$error_array = array(); //Holds error messages

if(isset($_POST['register_button'])){
	
	$name = strip_tags($_POST['fullname']); //Remove html tags
	$name = str_replace(' ', '', $name); //remove spaeletterces
	$name = ucfirst(strtolower($name)); //Uppercase first 
	
	$phone_number = strip_tags($_POST['number']); //Remove html tags
	$phone_number = str_replace(' ', '', $phone_number); //remove spaeletterces
	$phone_number = ucfirst(strtolower($phone_number)); //Uppercase first 
	
	$e_check_ph = mysqli_query($con, "SELECT phonenumber FROM user WHERE phonenumber='$phone_number'");
	
	//Count the number of rows returned
	$num_rows = mysqli_num_rows($e_check_ph);

	if($num_rows > 0) {
		array_push($error_array, "phonenumber already in use<br>");

	}

	
	$locality = strip_tags($_POST['locality']); //Remove html tags
	$locality = str_replace(' ', '', $locality); //remove spaeletterces
	$locality = ucfirst(strtolower($locality)); //Uppercase first 

	
		//email
	$em = strip_tags($_POST['email']); //Remove html tags
	$em = str_replace(' ', '', $em); //remove spaces
	$em = ucfirst(strtolower($em)); //Uppercase first letter
	


	//email 2
	$em2 = strip_tags($_POST['email-repeat']); //Remove html tags
	$em2 = str_replace(' ', '', $em2); //remove spaces
	$em2 = ucfirst(strtolower($em2)); //Uppercase first letter

	
		//Password
	$password = strip_tags($_POST['psw']); //Remove html tags
	$password2 = strip_tags($_POST['psw-repeat']); //Remove html tags
	
	$date = date("Y-m-d"); //Current date
	if($em == $em2) {
		
		//Check if email is in valid format 
		if(filter_var($em, FILTER_VALIDATE_EMAIL)) {
			
			$em = filter_var($em, FILTER_VALIDATE_EMAIL);

			//Check if email already exists 
			$e_check = mysqli_query($con, "SELECT email FROM user WHERE email='$em'");

			//Count the number of rows returned
			$num_rows = mysqli_num_rows($e_check);

			if($num_rows > 0) {
				array_push($error_array, "Email already in use<br>");

			}
		}
		else{
			array_push($error_array, "Invalid email format<br>");
		}
		
	}
	
	else{
		array_push($error_array, "Emails don't match<br>");
	}
	
	if($password != $password2) {
		array_push($error_array,  "Your passwords do not match<br>");
	}
	else {
		if(preg_match('/[^A-Za-z0-9]/', $password)) {
			array_push($error_array, "Your password can only contain english characters or numbers<br>");
		}
	}
	
	if(empty($error_array)) {
		$password = md5($password); //Encrypt password before sending to database

		//Generate username by concatenating name and phonenumber
		$id = strtolower($name . "_" . $phone_number);
		
		$query = mysqli_query($con, "INSERT INTO user VALUES ('', '$name', '$phone_number', '$id', '$locality', '$em', '$password')");

	}
	
	
	
	
}

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>

<form action="register.php" method="POST">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="fullname"><b>Full Name</b></label>
    <input type="text" placeholder="Name of the User" name="fullname" id="n1" >
	
	
	<label for="number"><b>Phone Number</b></label>
    <input type="text" placeholder="Number of the User" name="number" id = "n2" required>
	
	<?php if(in_array("phonenumber already in use<br>", $error_array)) echo "phonenumber already in use<br>"; ?>
	
	<label for="loality"><b>Locality</b></label>
    <input type="text" placeholder="Locality of the User" name="locality" required>
	
    
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>
	
	<label for="email"><b>Repeat Email</b></label>
    <input type="text" placeholder="Repeat Email" name="email-repeat" required>
	
	<?php if(in_array("Email already in use<br>", $error_array)) echo "Email already in use<br>"; 
		else if(in_array("Invalid email format<br>", $error_array)) echo "Invalid email format<br>";
		else if(in_array("Emails don't match<br>", $error_array)) echo "Emails don't match<br>"; ?>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>

    <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
	
	<?php if(in_array("Your passwords do not match<br>", $error_array)) echo "Your passwords do not match<br>"; 
		else if(in_array("Your password can only contain english characters or numbers<br>", $error_array)) echo "Your password can only contain english characters or numbers<br>";
	?>
    <hr>
    

    <input type="submit" class="registerbtn" name = "register_button" value = "Register">
  </div>
  
  <div class="container signin">
    <p>Already have an account? <a href="loginpage.html">Sign in</a>.</p>
  </div>
</form>

</body>
</html>




