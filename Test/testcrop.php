<?php 

use PHPUnit\Framework\Testcase;
require_once("config.php");
require_once("src\classes.php");

class testcrop extends TestCase
{
public function testgetcropDetails()
{
$ob=new Crop();
$honolulu=new Farmer();
$honolulu->Fid="f_101";
$rr=$ob->getCropDetails($honolulu);


$this->assertTrue($rr!=0);
}
public function testinsertCropDetails()
{
$ob=new Crop();
$res=$ob->insertcropDetails('f_103','alu','notun alu',date(),50);

$this->assertEquals($res,1);

}
public function testupdateDetails()
{
$ob=new Crop();
$res=$ob->updateDetails('f_102','Alu','Chandramukhi',date(),50);
$this->assertEquals($res,1);
}
public function testcropbuydetails()
{

$ob=new Crop();
$res=$ob->cropbuydetails('Alu','Chandramukhi',50);
$this->assertEquals($res,1);
}
}

?>